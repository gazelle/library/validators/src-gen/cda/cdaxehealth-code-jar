package net.ihe.gazelle.ips.datatypes;

import net.ihe.gazelle.ips.datatypes.BinaryDataEncoding;

public class DatatypesUtil {
	
	public DatatypesUtil() {}
	
	public static String stringValueOf(BinaryDataEncoding binaryDataEncoding){
		return net.ihe.gazelle.gen.common.CommonOperationsStatic.extractLitteralValue(binaryDataEncoding);
	}

	public static Object stringValueOf(Enum anElement2) {
		return net.ihe.gazelle.gen.common.CommonOperationsStatic.extractLitteralValue(anElement2);
	}

}
