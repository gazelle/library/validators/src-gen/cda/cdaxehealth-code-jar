/**
 * IVLMO.java
 *
 * File generated from the datatypes::IVLMO uml Class
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.ips.datatypes;

// End of user code
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;

import org.w3c.dom.Document;
import org.w3c.dom.Node;


/**
 * Description of the class IVLMO.
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IVL_MO", propOrder = {
	"low",
	"center",
	"width",
	"high"
})
@XmlRootElement(name = "IVL_MO")
public class IVLMO extends net.ihe.gazelle.ips.datatypes.SXCMMO implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	/**
	 *  The low limit of the interval. .
	 */
	@XmlElement(name = "low", namespace = "urn:hl7-org:v3")
	public net.ihe.gazelle.ips.datatypes.IVXBMO low;
	/**
	 *  The arithmetic mean of the interval (low plus high divided by 2). The purpose of distinguishing the center as a semantic property is for conversions of intervals from and to point values. .
	 */
	@XmlElement(name = "center", namespace = "urn:hl7-org:v3")
	public net.ihe.gazelle.ips.datatypes.MO center;
	/**
	 *  The difference between high and low boundary. The purpose of distinguishing a width property is to handle all cases of incomplete information symmetrically. In any interval representation only two of the three properties high, low, and width need to be stated and the third can be derived. .
	 */
	@XmlElement(name = "width", namespace = "urn:hl7-org:v3")
	public net.ihe.gazelle.ips.datatypes.MO width;
	/**
	 *  The high limit of the interval. .
	 */
	@XmlElement(name = "high", namespace = "urn:hl7-org:v3")
	public net.ihe.gazelle.ips.datatypes.IVXBMO high;
	
	/**
	 * An attribute containing marshalled element node
	 */
	@XmlTransient
	private Node _xmlNodePresentation;
	
	
	/**
	 * Return low.
	 * @return low :  The low limit of the interval. 
	 */
	public net.ihe.gazelle.ips.datatypes.IVXBMO getLow() {
	    return low;
	}
	
	/**
	 * Set a value to attribute low.
	 * @param low :  The low limit of the interval. .
	 */
	public void setLow(net.ihe.gazelle.ips.datatypes.IVXBMO low) {
	    this.low = low;
	}
	
	
	
	
	/**
	 * Return center.
	 * @return center :  The arithmetic mean of the interval (low plus high divided by 2). The purpose of distinguishing the center as a semantic property is for conversions of intervals from and to point values. 
	 */
	public net.ihe.gazelle.ips.datatypes.MO getCenter() {
	    return center;
	}
	
	/**
	 * Set a value to attribute center.
	 * @param center :  The arithmetic mean of the interval (low plus high divided by 2). The purpose of distinguishing the center as a semantic property is for conversions of intervals from and to point values. .
	 */
	public void setCenter(net.ihe.gazelle.ips.datatypes.MO center) {
	    this.center = center;
	}
	
	
	
	
	/**
	 * Return width.
	 * @return width :  The difference between high and low boundary. The purpose of distinguishing a width property is to handle all cases of incomplete information symmetrically. In any interval representation only two of the three properties high, low, and width need to be stated and the third can be derived. 
	 */
	public net.ihe.gazelle.ips.datatypes.MO getWidth() {
	    return width;
	}
	
	/**
	 * Set a value to attribute width.
	 * @param width :  The difference between high and low boundary. The purpose of distinguishing a width property is to handle all cases of incomplete information symmetrically. In any interval representation only two of the three properties high, low, and width need to be stated and the third can be derived. .
	 */
	public void setWidth(net.ihe.gazelle.ips.datatypes.MO width) {
	    this.width = width;
	}
	
	
	
	
	/**
	 * Return high.
	 * @return high :  The high limit of the interval. 
	 */
	public net.ihe.gazelle.ips.datatypes.IVXBMO getHigh() {
	    return high;
	}
	
	/**
	 * Set a value to attribute high.
	 * @param high :  The high limit of the interval. .
	 */
	public void setHigh(net.ihe.gazelle.ips.datatypes.IVXBMO high) {
	    this.high = high;
	}
	
	
	
	
	
	public Node get_xmlNodePresentation() {
		if (_xmlNodePresentation == null){
				JAXBContext jc;
				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
				dbf.setNamespaceAware(true);
				DocumentBuilder db = null;
				Document doc = null;
				try {
					db = dbf.newDocumentBuilder();
					doc = db.newDocument();
				} catch (ParserConfigurationException e1) {}
				try {
					jc = JAXBContext.newInstance("net.ihe.gazelle.ips.datatypes");
					Marshaller m = jc.createMarshaller();
					m.marshal(this, doc);
					_xmlNodePresentation = doc.getElementsByTagNameNS("urn:hl7-org:v3", "IVL_MO").item(0);
				} catch (JAXBException e) {
					try{
						db = dbf.newDocumentBuilder();
						_xmlNodePresentation = db.newDocument();
					}
					catch(Exception ee){}
				}
			}
			return _xmlNodePresentation;
	}
	
	public void set_xmlNodePresentation(Node _xmlNodePresentation) {
		this._xmlNodePresentation = _xmlNodePresentation;
	}
	
	
	

	
	/**
     * validate by a module of validation
     * 
     */
   public static void validateByModule(IVLMO iVLMO, String _location, ConstraintValidatorModule cvm, List<net.ihe.gazelle.validation.Notification> diagnostic){
   		if (iVLMO != null){
   			net.ihe.gazelle.ips.datatypes.SXCMMO.validateByModule(iVLMO, _location, cvm, diagnostic);
			net.ihe.gazelle.ips.datatypes.IVXBMO.validateByModule(iVLMO.getLow(), _location + "/low", cvm, diagnostic);
			net.ihe.gazelle.ips.datatypes.MO.validateByModule(iVLMO.getCenter(), _location + "/center", cvm, diagnostic);
			net.ihe.gazelle.ips.datatypes.MO.validateByModule(iVLMO.getWidth(), _location + "/width", cvm, diagnostic);
			net.ihe.gazelle.ips.datatypes.IVXBMO.validateByModule(iVLMO.getHigh(), _location + "/high", cvm, diagnostic);
    	}
    }

}