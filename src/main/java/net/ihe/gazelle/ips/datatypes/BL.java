/**
 * BL.java
 *
 * File generated from the datatypes::BL uml Class
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.ips.datatypes;

// End of user code
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;

import org.w3c.dom.Document;
import org.w3c.dom.Node;


/**
 * Description of the class BL.
 *
 * 
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BL", propOrder = {
	"value"
})
@XmlRootElement(name = "BL")
public class BL extends net.ihe.gazelle.ips.datatypes.ANY implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@XmlAttribute(name = "value")
	public Boolean value;
	
	/**
	 * An attribute containing marshalled element node
	 */
	@XmlTransient
	private Node _xmlNodePresentation;
	
	
	/**
	 * Return value.
	 * @return value
	 */
	public Boolean getValue() {
	    return value;
	}
	
	/**
	 * Set a value to attribute value.
	 * @param value.
	 */
	public void setValue(Boolean value) {
	    this.value = value;
	}
	
	
	/**
	 * Return value.
	 * @return value
	 * Generated for the use on jsf pages
	 */
	 @Deprecated
	public Boolean isValue() {
	    return value;
	}
	
	
	public Node get_xmlNodePresentation() {
		if (_xmlNodePresentation == null){
				JAXBContext jc;
				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
				dbf.setNamespaceAware(true);
				DocumentBuilder db = null;
				Document doc = null;
				try {
					db = dbf.newDocumentBuilder();
					doc = db.newDocument();
				} catch (ParserConfigurationException e1) {}
				try {
					jc = JAXBContext.newInstance("net.ihe.gazelle.ips.datatypes");
					Marshaller m = jc.createMarshaller();
					m.marshal(this, doc);
					_xmlNodePresentation = doc.getElementsByTagNameNS("urn:hl7-org:v3", "BL").item(0);
				} catch (JAXBException e) {
					try{
						db = dbf.newDocumentBuilder();
						_xmlNodePresentation = db.newDocument();
					}
					catch(Exception ee){}
				}
			}
			return _xmlNodePresentation;
	}
	
	public void set_xmlNodePresentation(Node _xmlNodePresentation) {
		this._xmlNodePresentation = _xmlNodePresentation;
	}
	
	
	

	
	/**
     * validate by a module of validation
     * 
     */
   public static void validateByModule(BL bL, String _location, ConstraintValidatorModule cvm, List<net.ihe.gazelle.validation.Notification> diagnostic){
   		if (bL != null){
   			net.ihe.gazelle.ips.datatypes.ANY.validateByModule(bL, _location, cvm, diagnostic);
    	}
    }

}