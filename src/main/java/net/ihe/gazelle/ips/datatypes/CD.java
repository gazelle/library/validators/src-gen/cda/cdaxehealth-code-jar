/**
 * CD.java
 *
 * File generated from the datatypes::CD uml Class
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.ips.datatypes;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import net.ihe.gazelle.ips.datatypes.ST;
import net.ihe.gazelle.gen.common.ConstraintValidatorModule;

import org.w3c.dom.Document;
import org.w3c.dom.Node;


/**
 * Description of the class CD.
 *
 * 
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CD", propOrder = {
	"designation",
	"originalText",
	"qualifier",
	"translation",
	"code",
	"codeSystem",
	"codeSystemName",
	"codeSystemVersion",
	"displayName"
})
@XmlRootElement(name = "CD")
public class CD extends net.ihe.gazelle.ips.datatypes.ANY implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	@XmlElement(
			name = "designation",
			namespace = "urn:hl7-org:ips"
	)
	public List<ST> designation;

	/**
	 *  The text or phrase used as the basis for the coding. .
	 */
	@XmlElement(name = "originalText", namespace = "urn:hl7-org:v3")
	public net.ihe.gazelle.ips.datatypes.ED originalText;
	/**
	 *  Specifies additional codes that increase the specificity of the primary code. .
	 */
	@XmlElement(name = "qualifier", namespace = "urn:hl7-org:v3")
	public List<net.ihe.gazelle.ips.datatypes.CR> qualifier;
	/**
	 *  A set of other concept descriptors that translate this concept descriptor into other code systems. .
	 */
	@XmlElement(name = "translation", namespace = "urn:hl7-org:v3")
	public List<net.ihe.gazelle.ips.datatypes.CD> translation;
	/**
	 *  The plain code symbol defined by the code system. For example, "784.0" is the code symbol of the ICD-9 code "784.0" for headache. .
	 */
	@XmlAttribute(name = "code")
	public String code;
	/**
	 *  Specifies the code system that defines the code. .
	 */
	@XmlAttribute(name = "codeSystem")
	public String codeSystem;
	/**
	 *  A common name of the coding system. .
	 */
	@XmlAttribute(name = "codeSystemName")
	public String codeSystemName;
	/**
	 *  If applicable, a version descriptor defined specifically for the given code system. .
	 */
	@XmlAttribute(name = "codeSystemVersion")
	public String codeSystemVersion;
	/**
	 *  A name or title for the code, under which the sending system shows the code value to its users. .
	 */
	@XmlAttribute(name = "displayName")
	public String displayName;
	
	/**
	 * An attribute containing marshalled element node
	 */
	@XmlTransient
	private Node _xmlNodePresentation;

	public List<ST> getDesignation() {
		if(this.designation == null){
			this.designation = new ArrayList<>();
		}
		return this.designation;
	}

	public void setDesignation(List<ST> designation) {
		this.designation = designation;
	}
	
	
	/**
	 * Return originalText.
	 * @return originalText :  The text or phrase used as the basis for the coding. 
	 */
	public net.ihe.gazelle.ips.datatypes.ED getOriginalText() {
	    return originalText;
	}
	
	/**
	 * Set a value to attribute originalText.
	 * @param originalText :  The text or phrase used as the basis for the coding. .
	 */
	public void setOriginalText(net.ihe.gazelle.ips.datatypes.ED originalText) {
	    this.originalText = originalText;
	}
	
	
	
	
	/**
	 * Return qualifier.
	 * @return qualifier :  Specifies additional codes that increase the specificity of the primary code. 
	 */
	public List<net.ihe.gazelle.ips.datatypes.CR> getQualifier() {
		if (qualifier == null) {
	        qualifier = new ArrayList<net.ihe.gazelle.ips.datatypes.CR>();
	    }
	    return qualifier;
	}
	
	/**
	 * Set a value to attribute qualifier.
	 * @param qualifier :  Specifies additional codes that increase the specificity of the primary code. .
	 */
	public void setQualifier(List<net.ihe.gazelle.ips.datatypes.CR> qualifier) {
	    this.qualifier = qualifier;
	}
	
	
	
	/**
	 * Add a qualifier to the qualifier collection.
	 * @param qualifier_elt Element to add.
	 */
	public void addQualifier(net.ihe.gazelle.ips.datatypes.CR qualifier_elt) {
	    this.getQualifier().add(qualifier_elt);
	}
	
	/**
	 * Remove a qualifier to the qualifier collection.
	 * @param qualifier_elt Element to remove
	 */
	public void removeQualifier(net.ihe.gazelle.ips.datatypes.CR qualifier_elt) {
	    this.getQualifier().remove(qualifier_elt);
	}
	
	/**
	 * Return translation.
	 * @return translation :  A set of other concept descriptors that translate this concept descriptor into other code systems. 
	 */
	public List<net.ihe.gazelle.ips.datatypes.CD> getTranslation() {
		if (translation == null) {
	        translation = new ArrayList<net.ihe.gazelle.ips.datatypes.CD>();
	    }
	    return translation;
	}
	
	/**
	 * Set a value to attribute translation.
	 * @param translation :  A set of other concept descriptors that translate this concept descriptor into other code systems. .
	 */
	public void setTranslation(List<net.ihe.gazelle.ips.datatypes.CD> translation) {
	    this.translation = translation;
	}
	
	
	
	/**
	 * Add a translation to the translation collection.
	 * @param translation_elt Element to add.
	 */
	public void addTranslation(net.ihe.gazelle.ips.datatypes.CD translation_elt) {
	    this.getTranslation().add(translation_elt);
	}
	
	/**
	 * Remove a translation to the translation collection.
	 * @param translation_elt Element to remove
	 */
	public void removeTranslation(net.ihe.gazelle.ips.datatypes.CD translation_elt) {
	    this.getTranslation().remove(translation_elt);
	}
	
	/**
	 * Return code.
	 * @return code :  The plain code symbol defined by the code system. For example, "784.0" is the code symbol of the ICD-9 code "784.0" for headache. 
	 */
	public String getCode() {
	    return code;
	}
	
	/**
	 * Set a value to attribute code.
	 * @param code :  The plain code symbol defined by the code system. For example, "784.0" is the code symbol of the ICD-9 code "784.0" for headache. .
	 */
	public void setCode(String code) {
	    this.code = code;
	}
	
	
	
	
	/**
	 * Return codeSystem.
	 * @return codeSystem :  Specifies the code system that defines the code. 
	 */
	public String getCodeSystem() {
	    return codeSystem;
	}
	
	/**
	 * Set a value to attribute codeSystem.
	 * @param codeSystem :  Specifies the code system that defines the code. .
	 */
	public void setCodeSystem(String codeSystem) {
	    this.codeSystem = codeSystem;
	}
	
	
	
	
	/**
	 * Return codeSystemName.
	 * @return codeSystemName :  A common name of the coding system. 
	 */
	public String getCodeSystemName() {
	    return codeSystemName;
	}
	
	/**
	 * Set a value to attribute codeSystemName.
	 * @param codeSystemName :  A common name of the coding system. .
	 */
	public void setCodeSystemName(String codeSystemName) {
	    this.codeSystemName = codeSystemName;
	}
	
	
	
	
	/**
	 * Return codeSystemVersion.
	 * @return codeSystemVersion :  If applicable, a version descriptor defined specifically for the given code system. 
	 */
	public String getCodeSystemVersion() {
	    return codeSystemVersion;
	}
	
	/**
	 * Set a value to attribute codeSystemVersion.
	 * @param codeSystemVersion :  If applicable, a version descriptor defined specifically for the given code system. .
	 */
	public void setCodeSystemVersion(String codeSystemVersion) {
	    this.codeSystemVersion = codeSystemVersion;
	}
	
	
	
	
	/**
	 * Return displayName.
	 * @return displayName :  A name or title for the code, under which the sending system shows the code value to its users. 
	 */
	public String getDisplayName() {
	    return displayName;
	}
	
	/**
	 * Set a value to attribute displayName.
	 * @param displayName :  A name or title for the code, under which the sending system shows the code value to its users. .
	 */
	public void setDisplayName(String displayName) {
	    this.displayName = displayName;
	}
	
	
	
	
	
	public Node get_xmlNodePresentation() {
		if (_xmlNodePresentation == null){
				JAXBContext jc;
				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
				dbf.setNamespaceAware(true);
				DocumentBuilder db = null;
				Document doc = null;
				try {
					db = dbf.newDocumentBuilder();
					doc = db.newDocument();
				} catch (ParserConfigurationException e1) {}
				try {
					jc = JAXBContext.newInstance("net.ihe.gazelle.ips.datatypes");
					Marshaller m = jc.createMarshaller();
					m.marshal(this, doc);
					_xmlNodePresentation = doc.getElementsByTagNameNS("urn:hl7-org:v3", "CD").item(0);
				} catch (JAXBException e) {
					try{
						db = dbf.newDocumentBuilder();
						_xmlNodePresentation = db.newDocument();
					}
					catch(Exception ee){}
				}
			}
			return _xmlNodePresentation;
	}
	
	public void set_xmlNodePresentation(Node _xmlNodePresentation) {
		this._xmlNodePresentation = _xmlNodePresentation;
	}
	
	
	
	/**
	 * Description of the method cdaEquals.
	 *
	 * @param other
	 * @return ret
	 */
	public Boolean cdaEquals(net.ihe.gazelle.ips.datatypes.CD other) {
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (codeSystem == null) {
			if (other.codeSystem != null)
				return false;
		} else if (!codeSystem.equals(other.codeSystem))
			return false;
		if (qualifier == null) {
			if (other.qualifier != null)
				return false;
		} else if (!qualifier.equals(other.qualifier))
			return false;
		return true;
	}
		

	
	/**
     * validate by a module of validation
     * 
     */
   public static void validateByModule(CD cD, String _location, ConstraintValidatorModule cvm, List<net.ihe.gazelle.validation.Notification> diagnostic){
   		if (cD != null){
   			net.ihe.gazelle.ips.datatypes.ANY.validateByModule(cD, _location, cvm, diagnostic);
			net.ihe.gazelle.ips.datatypes.ED.validateByModule(cD.getOriginalText(), _location + "/originalText", cvm, diagnostic);

			int i = 0;

			Iterator i$;

			for(i$ = cD.getDesignation().iterator(); i$.hasNext(); ++i) {
				ST designation = (ST)i$.next();
				ST.validateByModule(designation, _location + "/designation[" + i + "]", cvm, diagnostic);
			}

			i = 0;

			{
				for (net.ihe.gazelle.ips.datatypes.CR qualifier: cD.getQualifier()){
					net.ihe.gazelle.ips.datatypes.CR.validateByModule(qualifier, _location + "/qualifier[" + i + "]", cvm, diagnostic);
					i++;
				}
			}

			i = 0;

		   {
				for (net.ihe.gazelle.ips.datatypes.CD translation: cD.getTranslation()){
					net.ihe.gazelle.ips.datatypes.CD.validateByModule(translation, _location + "/translation[" + i + "]", cvm, diagnostic);
					i++;
				}
			}
			
    	}
    }

}