package net.ihe.gazelle.ips.datatypes;

import net.ihe.gazelle.ips.datatypes.*;
import net.ihe.gazelle.ips.datatypes.AD;
import net.ihe.gazelle.ips.datatypes.ADXP;
import net.ihe.gazelle.ips.datatypes.ANYNonNull;
import net.ihe.gazelle.ips.datatypes.AdxpCareOf;
import net.ihe.gazelle.ips.datatypes.AdxpCity;
import net.ihe.gazelle.ips.datatypes.AdxpCountry;
import net.ihe.gazelle.ips.datatypes.AdxpCounty;
import net.ihe.gazelle.ips.datatypes.AdxpDelimiter;
import net.ihe.gazelle.ips.datatypes.AdxpDirection;
import net.ihe.gazelle.ips.datatypes.AdxpPostBox;
import net.ihe.gazelle.ips.datatypes.AdxpPrecinct;
import net.ihe.gazelle.ips.datatypes.AdxpState;
import net.ihe.gazelle.ips.datatypes.AdxpUnitID;
import net.ihe.gazelle.ips.datatypes.AdxpUnitType;
import net.ihe.gazelle.ips.datatypes.BL;
import net.ihe.gazelle.ips.datatypes.BN;
import net.ihe.gazelle.ips.datatypes.BXITCD;
import net.ihe.gazelle.ips.datatypes.BXITIVLPQ;
import net.ihe.gazelle.ips.datatypes.CD;
import net.ihe.gazelle.ips.datatypes.CE;
import net.ihe.gazelle.ips.datatypes.CO;
import net.ihe.gazelle.ips.datatypes.CR;
import net.ihe.gazelle.ips.datatypes.CS;
import net.ihe.gazelle.ips.datatypes.CV;
import net.ihe.gazelle.ips.datatypes.DatatypesUtil;
import net.ihe.gazelle.ips.datatypes.ED;
import net.ihe.gazelle.ips.datatypes.EIVLEvent;
import net.ihe.gazelle.ips.datatypes.EIVLPPDTS;
import net.ihe.gazelle.ips.datatypes.EIVLTS;
import net.ihe.gazelle.ips.datatypes.EN;
import net.ihe.gazelle.ips.datatypes.ENXP;
import net.ihe.gazelle.ips.datatypes.EnDelimiter;
import net.ihe.gazelle.ips.datatypes.EnFamily;
import net.ihe.gazelle.ips.datatypes.EnGiven;
import net.ihe.gazelle.ips.datatypes.EnPrefix;
import net.ihe.gazelle.ips.datatypes.EnSuffix;
import net.ihe.gazelle.ips.datatypes.GLISTPQ;
import net.ihe.gazelle.ips.datatypes.GLISTTS;
import net.ihe.gazelle.ips.datatypes.HXITCE;
import net.ihe.gazelle.ips.datatypes.HXITPQ;
import net.ihe.gazelle.ips.datatypes.II;
import net.ihe.gazelle.ips.datatypes.INT;
import net.ihe.gazelle.ips.datatypes.IVLINT;
import net.ihe.gazelle.ips.datatypes.IVLMO;
import net.ihe.gazelle.ips.datatypes.IVLPPDPQ;
import net.ihe.gazelle.ips.datatypes.IVLPPDTS;
import net.ihe.gazelle.ips.datatypes.IVLPQ;
import net.ihe.gazelle.ips.datatypes.IVLREAL;
import net.ihe.gazelle.ips.datatypes.IVLTS;
import net.ihe.gazelle.ips.datatypes.IVXBINT;
import net.ihe.gazelle.ips.datatypes.IVXBMO;
import net.ihe.gazelle.ips.datatypes.IVXBPPDPQ;
import net.ihe.gazelle.ips.datatypes.IVXBPPDTS;
import net.ihe.gazelle.ips.datatypes.IVXBPQ;
import net.ihe.gazelle.ips.datatypes.IVXBREAL;
import net.ihe.gazelle.ips.datatypes.IVXBTS;
import net.ihe.gazelle.ips.datatypes.MO;
import net.ihe.gazelle.ips.datatypes.ON;
import net.ihe.gazelle.ips.datatypes.PIVLPPDTS;
import net.ihe.gazelle.ips.datatypes.PIVLTS;
import net.ihe.gazelle.ips.datatypes.PN;
import net.ihe.gazelle.ips.datatypes.PPDPQ;
import net.ihe.gazelle.ips.datatypes.PPDTS;
import net.ihe.gazelle.ips.datatypes.PQ;
import net.ihe.gazelle.ips.datatypes.PQR;
import net.ihe.gazelle.ips.datatypes.REAL;
import net.ihe.gazelle.ips.datatypes.RTO;
import net.ihe.gazelle.ips.datatypes.RTOMOPQ;
import net.ihe.gazelle.ips.datatypes.RTOPQPQ;
import net.ihe.gazelle.ips.datatypes.RTOQTYQTY;
import net.ihe.gazelle.ips.datatypes.SC;
import net.ihe.gazelle.ips.datatypes.SLISTPQ;
import net.ihe.gazelle.ips.datatypes.SLISTTS;
import net.ihe.gazelle.ips.datatypes.ST;
import net.ihe.gazelle.ips.datatypes.SXCMCD;
import net.ihe.gazelle.ips.datatypes.SXCMINT;
import net.ihe.gazelle.ips.datatypes.SXCMMO;
import net.ihe.gazelle.ips.datatypes.SXCMPPDPQ;
import net.ihe.gazelle.ips.datatypes.SXCMPPDTS;
import net.ihe.gazelle.ips.datatypes.SXCMPQ;
import net.ihe.gazelle.ips.datatypes.SXCMREAL;
import net.ihe.gazelle.ips.datatypes.SXCMTS;
import net.ihe.gazelle.ips.datatypes.SXPRTS;
import net.ihe.gazelle.ips.datatypes.TEL;
import net.ihe.gazelle.ips.datatypes.TN;
import net.ihe.gazelle.ips.datatypes.TS;
import net.ihe.gazelle.ips.datatypes.Thumbnail;
import net.ihe.gazelle.ips.datatypes.UVPTS;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

@XmlRegistry
public class ObjectFactory {

	private final static QName _ADGroup_QNAME = new QName("", "group:2");
	private final static QName _ADDelimiter_QNAME = new QName("urn:hl7-org:v3", "delimiter");
	private final static QName _ADCountry_QNAME = new QName("urn:hl7-org:v3", "country");
	private final static QName _ADState_QNAME = new QName("urn:hl7-org:v3", "state");
	private final static QName _ADCounty_QNAME = new QName("urn:hl7-org:v3", "county");
	private final static QName _ADCity_QNAME = new QName("urn:hl7-org:v3", "city");
	private final static QName _ADPostalCode_QNAME = new QName("urn:hl7-org:v3", "postalCode");
	private final static QName _ADStreetAddressLine_QNAME = new QName("urn:hl7-org:v3", "streetAddressLine");
	private final static QName _ADHouseNumber_QNAME = new QName("urn:hl7-org:v3", "houseNumber");
	private final static QName _ADHouseNumberNumeric_QNAME = new QName("urn:hl7-org:v3", "houseNumberNumeric");
	private final static QName _ADDirection_QNAME = new QName("urn:hl7-org:v3", "direction");
	private final static QName _ADStreetName_QNAME = new QName("urn:hl7-org:v3", "streetName");
	private final static QName _ADStreetNameBase_QNAME = new QName("urn:hl7-org:v3", "streetNameBase");
	private final static QName _ADStreetNameType_QNAME = new QName("urn:hl7-org:v3", "streetNameType");
	private final static QName _ADAdditionalLocator_QNAME = new QName("urn:hl7-org:v3", "additionalLocator");
	private final static QName _ADUnitID_QNAME = new QName("urn:hl7-org:v3", "unitID");
	private final static QName _ADUnitType_QNAME = new QName("urn:hl7-org:v3", "unitType");
	private final static QName _ADCareOf_QNAME = new QName("urn:hl7-org:v3", "careOf");
	private final static QName _ADCensusTract_QNAME = new QName("urn:hl7-org:v3", "censusTract");
	private final static QName _ADDeliveryAddressLine_QNAME = new QName("urn:hl7-org:v3", "deliveryAddressLine");
	private final static QName _ADDeliveryInstallationType_QNAME = new QName("urn:hl7-org:v3", "deliveryInstallationType");
	private final static QName _ADDeliveryInstallationArea_QNAME = new QName("urn:hl7-org:v3", "deliveryInstallationArea");
	private final static QName _ADDeliveryInstallationQualifier_QNAME = new QName("urn:hl7-org:v3", "deliveryInstallationQualifier");
	private final static QName _ADDeliveryMode_QNAME = new QName("urn:hl7-org:v3", "deliveryMode");
	private final static QName _ADDeliveryModeIdentifier_QNAME = new QName("urn:hl7-org:v3", "deliveryModeIdentifier");
	private final static QName _ADBuildingNumberSuffix_QNAME = new QName("urn:hl7-org:v3", "buildingNumberSuffix");
	private final static QName _ADPostBox_QNAME = new QName("urn:hl7-org:v3", "postBox");
	private final static QName _ADPrecinct_QNAME = new QName("urn:hl7-org:v3", "precinct");
	private final static QName _ADUseablePeriod_QNAME = new QName("urn:hl7-org:v3", "useablePeriod");
	private final static QName _ENGroup_QNAME = new QName("", "group:2");
	private final static QName _ENDelimiter_QNAME = new QName("urn:hl7-org:v3", "delimiter");
	private final static QName _ENFamily_QNAME = new QName("urn:hl7-org:v3", "family");
	private final static QName _ENGiven_QNAME = new QName("urn:hl7-org:v3", "given");
	private final static QName _ENPrefix_QNAME = new QName("urn:hl7-org:v3", "prefix");
	private final static QName _ENSuffix_QNAME = new QName("urn:hl7-org:v3", "suffix");
	private final static QName _ENValidTime_QNAME = new QName("urn:hl7-org:v3", "validTime");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public ObjectFactory() {}
    
    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.AD}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.AD createAD() {
        return new net.ihe.gazelle.ips.datatypes.AD();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.AdxpDelimiter}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.AdxpDelimiter createAdxpDelimiter() {
        return new net.ihe.gazelle.ips.datatypes.AdxpDelimiter();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.ADXP}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.ADXP createADXP() {
        return new ADXP();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.ST}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.ST createST() {
        return new ST();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.ED}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.ED createED() {
        return new ED();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.TEL}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.TEL createTEL() {
        return new TEL();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.SXCMTS}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.SXCMTS createSXCMTS() {
        return new net.ihe.gazelle.ips.datatypes.SXCMTS();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.TS}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.TS createTS() {
        return new TS();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.Thumbnail}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.Thumbnail createThumbnail() {
        return new Thumbnail();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.AdxpCountry}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.AdxpCountry createAdxpCountry() {
        return new net.ihe.gazelle.ips.datatypes.AdxpCountry();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.AdxpState}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.AdxpState createAdxpState() {
        return new net.ihe.gazelle.ips.datatypes.AdxpState();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.AdxpCounty}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.AdxpCounty createAdxpCounty() {
        return new net.ihe.gazelle.ips.datatypes.AdxpCounty();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.AdxpCity}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.AdxpCity createAdxpCity() {
        return new net.ihe.gazelle.ips.datatypes.AdxpCity();
    }

    /**
     * Create an instance of {@link  AdxpPostalCode}
     * 
     */
    public AdxpPostalCode createAdxpPostalCode() {
        return new AdxpPostalCode();
    }

    /**
     * Create an instance of {@link  AdxpStreetAddressLine}
     * 
     */
    public AdxpStreetAddressLine createAdxpStreetAddressLine() {
        return new AdxpStreetAddressLine();
    }

    /**
     * Create an instance of {@link  AdxpHouseNumber}
     * 
     */
    public AdxpHouseNumber createAdxpHouseNumber() {
        return new AdxpHouseNumber();
    }

    /**
     * Create an instance of {@link  AdxpHouseNumberNumeric}
     * 
     */
    public AdxpHouseNumberNumeric createAdxpHouseNumberNumeric() {
        return new AdxpHouseNumberNumeric();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.AdxpDirection}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.AdxpDirection createAdxpDirection() {
        return new net.ihe.gazelle.ips.datatypes.AdxpDirection();
    }

    /**
     * Create an instance of {@link  AdxpStreetName}
     * 
     */
    public AdxpStreetName createAdxpStreetName() {
        return new AdxpStreetName();
    }

    /**
     * Create an instance of {@link  AdxpStreetNameBase}
     * 
     */
    public AdxpStreetNameBase createAdxpStreetNameBase() {
        return new AdxpStreetNameBase();
    }

    /**
     * Create an instance of {@link  AdxpStreetNameType}
     * 
     */
    public AdxpStreetNameType createAdxpStreetNameType() {
        return new AdxpStreetNameType();
    }

    /**
     * Create an instance of {@link  AdxpAdditionalLocator}
     * 
     */
    public AdxpAdditionalLocator createAdxpAdditionalLocator() {
        return new AdxpAdditionalLocator();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.AdxpUnitID}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.AdxpUnitID createAdxpUnitID() {
        return new net.ihe.gazelle.ips.datatypes.AdxpUnitID();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.AdxpUnitType}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.AdxpUnitType createAdxpUnitType() {
        return new net.ihe.gazelle.ips.datatypes.AdxpUnitType();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.AdxpCareOf}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.AdxpCareOf createAdxpCareOf() {
        return new net.ihe.gazelle.ips.datatypes.AdxpCareOf();
    }

    /**
     * Create an instance of {@link  AdxpCensusTract}
     * 
     */
    public AdxpCensusTract createAdxpCensusTract() {
        return new AdxpCensusTract();
    }

    /**
     * Create an instance of {@link  AdxpDeliveryAddressLine}
     * 
     */
    public AdxpDeliveryAddressLine createAdxpDeliveryAddressLine() {
        return new AdxpDeliveryAddressLine();
    }

    /**
     * Create an instance of {@link  AdxpDeliveryInstallationType}
     * 
     */
    public AdxpDeliveryInstallationType createAdxpDeliveryInstallationType() {
        return new AdxpDeliveryInstallationType();
    }

    /**
     * Create an instance of {@link  AdxpDeliveryInstallationArea}
     * 
     */
    public AdxpDeliveryInstallationArea createAdxpDeliveryInstallationArea() {
        return new AdxpDeliveryInstallationArea();
    }

    /**
     * Create an instance of {@link  AdxpDeliveryInstallationQualifier}
     * 
     */
    public AdxpDeliveryInstallationQualifier createAdxpDeliveryInstallationQualifier() {
        return new AdxpDeliveryInstallationQualifier();
    }

    /**
     * Create an instance of {@link  AdxpDeliveryMode}
     * 
     */
    public AdxpDeliveryMode createAdxpDeliveryMode() {
        return new AdxpDeliveryMode();
    }

    /**
     * Create an instance of {@link  AdxpDeliveryModeIdentifier}
     * 
     */
    public AdxpDeliveryModeIdentifier createAdxpDeliveryModeIdentifier() {
        return new AdxpDeliveryModeIdentifier();
    }

    /**
     * Create an instance of {@link  AdxpBuildingNumberSuffix}
     * 
     */
    public AdxpBuildingNumberSuffix createAdxpBuildingNumberSuffix() {
        return new AdxpBuildingNumberSuffix();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.AdxpPostBox}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.AdxpPostBox createAdxpPostBox() {
        return new net.ihe.gazelle.ips.datatypes.AdxpPostBox();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.AdxpPrecinct}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.AdxpPrecinct createAdxpPrecinct() {
        return new net.ihe.gazelle.ips.datatypes.AdxpPrecinct();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.ANYNonNull}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.ANYNonNull createANYNonNull() {
        return new ANYNonNull();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.BL}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.BL createBL() {
        return new BL();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.BN}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.BN createBN() {
        return new BN();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.BXITCD}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.BXITCD createBXITCD() {
        return new BXITCD();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.CD}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.CD createCD() {
        return new CD();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.CR}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.CR createCR() {
        return new CR();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.CV}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.CV createCV() {
        return new CV();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.CE}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.CE createCE() {
        return new CE();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.BXITIVLPQ}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.BXITIVLPQ createBXITIVLPQ() {
        return new BXITIVLPQ();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.IVLPQ}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.IVLPQ createIVLPQ() {
        return new IVLPQ();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.SXCMPQ}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.SXCMPQ createSXCMPQ() {
        return new SXCMPQ();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.PQ}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.PQ createPQ() {
        return new PQ();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.PQR}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.PQR createPQR() {
        return new PQR();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.IVXBPQ}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.IVXBPQ createIVXBPQ() {
        return new IVXBPQ();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.CO}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.CO createCO() {
        return new CO();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.CS}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.CS createCS() {
        return new CS();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.EIVLEvent}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.EIVLEvent createEIVLEvent() {
        return new EIVLEvent();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.EIVLPPDTS}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.EIVLPPDTS createEIVLPPDTS() {
        return new EIVLPPDTS();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.SXCMPPDTS}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.SXCMPPDTS createSXCMPPDTS() {
        return new SXCMPPDTS();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.PPDTS}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.PPDTS createPPDTS() {
        return new PPDTS();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.IVLPPDPQ}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.IVLPPDPQ createIVLPPDPQ() {
        return new IVLPPDPQ();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.SXCMPPDPQ}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.SXCMPPDPQ createSXCMPPDPQ() {
        return new SXCMPPDPQ();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.PPDPQ}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.PPDPQ createPPDPQ() {
        return new PPDPQ();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.IVXBPPDPQ}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.IVXBPPDPQ createIVXBPPDPQ() {
        return new IVXBPPDPQ();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.EIVLTS}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.EIVLTS createEIVLTS() {
        return new EIVLTS();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.EN}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.EN createEN() {
        return new net.ihe.gazelle.ips.datatypes.EN();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.EnDelimiter}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.EnDelimiter createEnDelimiter() {
        return new net.ihe.gazelle.ips.datatypes.EnDelimiter();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.ENXP}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.ENXP createENXP() {
        return new ENXP();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.EnFamily}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.EnFamily createEnFamily() {
        return new net.ihe.gazelle.ips.datatypes.EnFamily();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.EnGiven}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.EnGiven createEnGiven() {
        return new net.ihe.gazelle.ips.datatypes.EnGiven();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.EnPrefix}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.EnPrefix createEnPrefix() {
        return new net.ihe.gazelle.ips.datatypes.EnPrefix();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.EnSuffix}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.EnSuffix createEnSuffix() {
        return new net.ihe.gazelle.ips.datatypes.EnSuffix();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.IVLTS}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.IVLTS createIVLTS() {
        return new net.ihe.gazelle.ips.datatypes.IVLTS();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.IVXBTS}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.IVXBTS createIVXBTS() {
        return new IVXBTS();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.GLISTPQ}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.GLISTPQ createGLISTPQ() {
        return new GLISTPQ();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.GLISTTS}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.GLISTTS createGLISTTS() {
        return new GLISTTS();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.HXITCE}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.HXITCE createHXITCE() {
        return new HXITCE();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.HXITPQ}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.HXITPQ createHXITPQ() {
        return new HXITPQ();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.II}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.II createII() {
        return new II();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.INT}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.INT createINT() {
        return new INT();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.IVLINT}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.IVLINT createIVLINT() {
        return new IVLINT();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.SXCMINT}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.SXCMINT createSXCMINT() {
        return new SXCMINT();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.IVXBINT}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.IVXBINT createIVXBINT() {
        return new IVXBINT();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.IVLMO}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.IVLMO createIVLMO() {
        return new IVLMO();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.SXCMMO}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.SXCMMO createSXCMMO() {
        return new SXCMMO();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.MO}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.MO createMO() {
        return new MO();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.IVXBMO}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.IVXBMO createIVXBMO() {
        return new IVXBMO();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.IVLPPDTS}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.IVLPPDTS createIVLPPDTS() {
        return new IVLPPDTS();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.IVXBPPDTS}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.IVXBPPDTS createIVXBPPDTS() {
        return new IVXBPPDTS();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.IVLREAL}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.IVLREAL createIVLREAL() {
        return new IVLREAL();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.SXCMREAL}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.SXCMREAL createSXCMREAL() {
        return new SXCMREAL();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.REAL}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.REAL createREAL() {
        return new REAL();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.IVXBREAL}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.IVXBREAL createIVXBREAL() {
        return new IVXBREAL();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.ON}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.ON createON() {
        return new ON();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.PIVLPPDTS}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.PIVLPPDTS createPIVLPPDTS() {
        return new PIVLPPDTS();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.PIVLTS}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.PIVLTS createPIVLTS() {
        return new PIVLTS();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.PN}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.PN createPN() {
        return new PN();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.RTO}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.RTO createRTO() {
        return new RTO();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.RTOQTYQTY}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.RTOQTYQTY createRTOQTYQTY() {
        return new RTOQTYQTY();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.RTOMOPQ}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.RTOMOPQ createRTOMOPQ() {
        return new RTOMOPQ();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.RTOPQPQ}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.RTOPQPQ createRTOPQPQ() {
        return new RTOPQPQ();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.SC}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.SC createSC() {
        return new SC();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.SLISTPQ}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.SLISTPQ createSLISTPQ() {
        return new SLISTPQ();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.SLISTTS}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.SLISTTS createSLISTTS() {
        return new SLISTTS();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.SXCMCD}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.SXCMCD createSXCMCD() {
        return new SXCMCD();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.SXPRTS}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.SXPRTS createSXPRTS() {
        return new SXPRTS();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.TN}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.TN createTN() {
        return new TN();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.UVPTS}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.UVPTS createUVPTS() {
        return new UVPTS();
    }

    /**
     * Create an instance of {@link  net.ihe.gazelle.ips.datatypes.DatatypesUtil}
     * 
     */
    public net.ihe.gazelle.ips.datatypes.DatatypesUtil createDatatypesUtil() {
        return new DatatypesUtil();
    }



	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpDelimiter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:hl7-org:v3", name = "delimiter", scope = net.ihe.gazelle.ips.datatypes.AD.class)
    public JAXBElement<AdxpDelimiter> createADDelimiter(AdxpDelimiter value) {
        return new JAXBElement<AdxpDelimiter>(_ADDelimiter_QNAME, AdxpDelimiter.class, net.ihe.gazelle.ips.datatypes.AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpCountry }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:hl7-org:v3", name = "country", scope = net.ihe.gazelle.ips.datatypes.AD.class)
    public JAXBElement<AdxpCountry> createADCountry(AdxpCountry value) {
        return new JAXBElement<AdxpCountry>(_ADCountry_QNAME, AdxpCountry.class, net.ihe.gazelle.ips.datatypes.AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpState }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:hl7-org:v3", name = "state", scope = net.ihe.gazelle.ips.datatypes.AD.class)
    public JAXBElement<AdxpState> createADState(AdxpState value) {
        return new JAXBElement<AdxpState>(_ADState_QNAME, AdxpState.class, net.ihe.gazelle.ips.datatypes.AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpCounty }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:hl7-org:v3", name = "county", scope = net.ihe.gazelle.ips.datatypes.AD.class)
    public JAXBElement<AdxpCounty> createADCounty(AdxpCounty value) {
        return new JAXBElement<AdxpCounty>(_ADCounty_QNAME, AdxpCounty.class, net.ihe.gazelle.ips.datatypes.AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpCity }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:hl7-org:v3", name = "city", scope = net.ihe.gazelle.ips.datatypes.AD.class)
    public JAXBElement<AdxpCity> createADCity(AdxpCity value) {
        return new JAXBElement<AdxpCity>(_ADCity_QNAME, AdxpCity.class, net.ihe.gazelle.ips.datatypes.AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpPostalCode }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:hl7-org:v3", name = "postalCode", scope = net.ihe.gazelle.ips.datatypes.AD.class)
    public JAXBElement<AdxpPostalCode> createADPostalCode(AdxpPostalCode value) {
        return new JAXBElement<AdxpPostalCode>(_ADPostalCode_QNAME, AdxpPostalCode.class, net.ihe.gazelle.ips.datatypes.AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpStreetAddressLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:hl7-org:v3", name = "streetAddressLine", scope = net.ihe.gazelle.ips.datatypes.AD.class)
    public JAXBElement<AdxpStreetAddressLine> createADStreetAddressLine(AdxpStreetAddressLine value) {
        return new JAXBElement<AdxpStreetAddressLine>(_ADStreetAddressLine_QNAME, AdxpStreetAddressLine.class, net.ihe.gazelle.ips.datatypes.AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpHouseNumber }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:hl7-org:v3", name = "houseNumber", scope = net.ihe.gazelle.ips.datatypes.AD.class)
    public JAXBElement<AdxpHouseNumber> createADHouseNumber(AdxpHouseNumber value) {
        return new JAXBElement<AdxpHouseNumber>(_ADHouseNumber_QNAME, AdxpHouseNumber.class, net.ihe.gazelle.ips.datatypes.AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpHouseNumberNumeric }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:hl7-org:v3", name = "houseNumberNumeric", scope = net.ihe.gazelle.ips.datatypes.AD.class)
    public JAXBElement<AdxpHouseNumberNumeric> createADHouseNumberNumeric(AdxpHouseNumberNumeric value) {
        return new JAXBElement<AdxpHouseNumberNumeric>(_ADHouseNumberNumeric_QNAME, AdxpHouseNumberNumeric.class, net.ihe.gazelle.ips.datatypes.AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpDirection }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:hl7-org:v3", name = "direction", scope = net.ihe.gazelle.ips.datatypes.AD.class)
    public JAXBElement<AdxpDirection> createADDirection(AdxpDirection value) {
        return new JAXBElement<AdxpDirection>(_ADDirection_QNAME, AdxpDirection.class, net.ihe.gazelle.ips.datatypes.AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpStreetName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:hl7-org:v3", name = "streetName", scope = net.ihe.gazelle.ips.datatypes.AD.class)
    public JAXBElement<AdxpStreetName> createADStreetName(AdxpStreetName value) {
        return new JAXBElement<AdxpStreetName>(_ADStreetName_QNAME, AdxpStreetName.class, net.ihe.gazelle.ips.datatypes.AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpStreetNameBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:hl7-org:v3", name = "streetNameBase", scope = net.ihe.gazelle.ips.datatypes.AD.class)
    public JAXBElement<AdxpStreetNameBase> createADStreetNameBase(AdxpStreetNameBase value) {
        return new JAXBElement<AdxpStreetNameBase>(_ADStreetNameBase_QNAME, AdxpStreetNameBase.class, net.ihe.gazelle.ips.datatypes.AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpStreetNameType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:hl7-org:v3", name = "streetNameType", scope = net.ihe.gazelle.ips.datatypes.AD.class)
    public JAXBElement<AdxpStreetNameType> createADStreetNameType(AdxpStreetNameType value) {
        return new JAXBElement<AdxpStreetNameType>(_ADStreetNameType_QNAME, AdxpStreetNameType.class, net.ihe.gazelle.ips.datatypes.AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpAdditionalLocator }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:hl7-org:v3", name = "additionalLocator", scope = net.ihe.gazelle.ips.datatypes.AD.class)
    public JAXBElement<AdxpAdditionalLocator> createADAdditionalLocator(AdxpAdditionalLocator value) {
        return new JAXBElement<AdxpAdditionalLocator>(_ADAdditionalLocator_QNAME, AdxpAdditionalLocator.class, net.ihe.gazelle.ips.datatypes.AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpUnitID }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:hl7-org:v3", name = "unitID", scope = net.ihe.gazelle.ips.datatypes.AD.class)
    public JAXBElement<AdxpUnitID> createADUnitID(AdxpUnitID value) {
        return new JAXBElement<AdxpUnitID>(_ADUnitID_QNAME, AdxpUnitID.class, net.ihe.gazelle.ips.datatypes.AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpUnitType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:hl7-org:v3", name = "unitType", scope = net.ihe.gazelle.ips.datatypes.AD.class)
    public JAXBElement<AdxpUnitType> createADUnitType(AdxpUnitType value) {
        return new JAXBElement<AdxpUnitType>(_ADUnitType_QNAME, AdxpUnitType.class, net.ihe.gazelle.ips.datatypes.AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpCareOf }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:hl7-org:v3", name = "careOf", scope = net.ihe.gazelle.ips.datatypes.AD.class)
    public JAXBElement<AdxpCareOf> createADCareOf(AdxpCareOf value) {
        return new JAXBElement<AdxpCareOf>(_ADCareOf_QNAME, AdxpCareOf.class, net.ihe.gazelle.ips.datatypes.AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpCensusTract }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:hl7-org:v3", name = "censusTract", scope = net.ihe.gazelle.ips.datatypes.AD.class)
    public JAXBElement<AdxpCensusTract> createADCensusTract(AdxpCensusTract value) {
        return new JAXBElement<AdxpCensusTract>(_ADCensusTract_QNAME, AdxpCensusTract.class, net.ihe.gazelle.ips.datatypes.AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpDeliveryAddressLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:hl7-org:v3", name = "deliveryAddressLine", scope = net.ihe.gazelle.ips.datatypes.AD.class)
    public JAXBElement<AdxpDeliveryAddressLine> createADDeliveryAddressLine(AdxpDeliveryAddressLine value) {
        return new JAXBElement<AdxpDeliveryAddressLine>(_ADDeliveryAddressLine_QNAME, AdxpDeliveryAddressLine.class, net.ihe.gazelle.ips.datatypes.AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpDeliveryInstallationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:hl7-org:v3", name = "deliveryInstallationType", scope = net.ihe.gazelle.ips.datatypes.AD.class)
    public JAXBElement<AdxpDeliveryInstallationType> createADDeliveryInstallationType(AdxpDeliveryInstallationType value) {
        return new JAXBElement<AdxpDeliveryInstallationType>(_ADDeliveryInstallationType_QNAME, AdxpDeliveryInstallationType.class, net.ihe.gazelle.ips.datatypes.AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpDeliveryInstallationArea }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:hl7-org:v3", name = "deliveryInstallationArea", scope = net.ihe.gazelle.ips.datatypes.AD.class)
    public JAXBElement<AdxpDeliveryInstallationArea> createADDeliveryInstallationArea(AdxpDeliveryInstallationArea value) {
        return new JAXBElement<AdxpDeliveryInstallationArea>(_ADDeliveryInstallationArea_QNAME, AdxpDeliveryInstallationArea.class, net.ihe.gazelle.ips.datatypes.AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpDeliveryInstallationQualifier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:hl7-org:v3", name = "deliveryInstallationQualifier", scope = net.ihe.gazelle.ips.datatypes.AD.class)
    public JAXBElement<AdxpDeliveryInstallationQualifier> createADDeliveryInstallationQualifier(AdxpDeliveryInstallationQualifier value) {
        return new JAXBElement<AdxpDeliveryInstallationQualifier>(_ADDeliveryInstallationQualifier_QNAME, AdxpDeliveryInstallationQualifier.class, net.ihe.gazelle.ips.datatypes.AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpDeliveryMode }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:hl7-org:v3", name = "deliveryMode", scope = net.ihe.gazelle.ips.datatypes.AD.class)
    public JAXBElement<AdxpDeliveryMode> createADDeliveryMode(AdxpDeliveryMode value) {
        return new JAXBElement<AdxpDeliveryMode>(_ADDeliveryMode_QNAME, AdxpDeliveryMode.class, net.ihe.gazelle.ips.datatypes.AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpDeliveryModeIdentifier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:hl7-org:v3", name = "deliveryModeIdentifier", scope = net.ihe.gazelle.ips.datatypes.AD.class)
    public JAXBElement<AdxpDeliveryModeIdentifier> createADDeliveryModeIdentifier(AdxpDeliveryModeIdentifier value) {
        return new JAXBElement<AdxpDeliveryModeIdentifier>(_ADDeliveryModeIdentifier_QNAME, AdxpDeliveryModeIdentifier.class, net.ihe.gazelle.ips.datatypes.AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpBuildingNumberSuffix }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:hl7-org:v3", name = "buildingNumberSuffix", scope = net.ihe.gazelle.ips.datatypes.AD.class)
    public JAXBElement<AdxpBuildingNumberSuffix> createADBuildingNumberSuffix(AdxpBuildingNumberSuffix value) {
        return new JAXBElement<AdxpBuildingNumberSuffix>(_ADBuildingNumberSuffix_QNAME, AdxpBuildingNumberSuffix.class, net.ihe.gazelle.ips.datatypes.AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpPostBox }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:hl7-org:v3", name = "postBox", scope = net.ihe.gazelle.ips.datatypes.AD.class)
    public JAXBElement<AdxpPostBox> createADPostBox(AdxpPostBox value) {
        return new JAXBElement<AdxpPostBox>(_ADPostBox_QNAME, AdxpPostBox.class, net.ihe.gazelle.ips.datatypes.AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdxpPrecinct }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:hl7-org:v3", name = "precinct", scope = net.ihe.gazelle.ips.datatypes.AD.class)
    public JAXBElement<AdxpPrecinct> createADPrecinct(AdxpPrecinct value) {
        return new JAXBElement<AdxpPrecinct>(_ADPrecinct_QNAME, AdxpPrecinct.class, net.ihe.gazelle.ips.datatypes.AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link SXCMTS }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:hl7-org:v3", name = "useablePeriod", scope = net.ihe.gazelle.ips.datatypes.AD.class)
    public JAXBElement<SXCMTS> createADUseablePeriod(SXCMTS value) {
        return new JAXBElement<SXCMTS>(_ADUseablePeriod_QNAME, SXCMTS.class, AD.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnDelimiter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:hl7-org:v3", name = "delimiter", scope = net.ihe.gazelle.ips.datatypes.EN.class)
    public JAXBElement<EnDelimiter> createENDelimiter(EnDelimiter value) {
        return new JAXBElement<EnDelimiter>(_ENDelimiter_QNAME, EnDelimiter.class, net.ihe.gazelle.ips.datatypes.EN.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnFamily }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:hl7-org:v3", name = "family", scope = net.ihe.gazelle.ips.datatypes.EN.class)
    public JAXBElement<EnFamily> createENFamily(EnFamily value) {
        return new JAXBElement<EnFamily>(_ENFamily_QNAME, EnFamily.class, net.ihe.gazelle.ips.datatypes.EN.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnGiven }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:hl7-org:v3", name = "given", scope = net.ihe.gazelle.ips.datatypes.EN.class)
    public JAXBElement<EnGiven> createENGiven(EnGiven value) {
        return new JAXBElement<EnGiven>(_ENGiven_QNAME, EnGiven.class, net.ihe.gazelle.ips.datatypes.EN.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnPrefix }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:hl7-org:v3", name = "prefix", scope = net.ihe.gazelle.ips.datatypes.EN.class)
    public JAXBElement<EnPrefix> createENPrefix(EnPrefix value) {
        return new JAXBElement<EnPrefix>(_ENPrefix_QNAME, EnPrefix.class, net.ihe.gazelle.ips.datatypes.EN.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnSuffix }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:hl7-org:v3", name = "suffix", scope = net.ihe.gazelle.ips.datatypes.EN.class)
    public JAXBElement<EnSuffix> createENSuffix(EnSuffix value) {
        return new JAXBElement<EnSuffix>(_ENSuffix_QNAME, EnSuffix.class, net.ihe.gazelle.ips.datatypes.EN.class, value);
    }
	/**
     * Create an instance of {@link JAXBElement }{@code <}{@link IVLTS }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:hl7-org:v3", name = "validTime", scope = net.ihe.gazelle.ips.datatypes.EN.class)
    public JAXBElement<IVLTS> createENValidTime(IVLTS value) {
        return new JAXBElement<IVLTS>(_ENValidTime_QNAME, IVLTS.class, EN.class, value);
    }

}