/**
 * IVXBTS.java
 *
 * File generated from the datatypes::IVXBTS uml Class
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.ips.datatypes;

// End of user code
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;

import org.w3c.dom.Document;
import org.w3c.dom.Node;


/**
 * Description of the class IVXBTS.
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IVXB_TS", propOrder = {
	"inclusive"
})
@XmlRootElement(name = "IVXB_TS")
public class IVXBTS extends net.ihe.gazelle.ips.datatypes.TS implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	/**
	 *  Specifies whether the limit is included in the interval (interval is closed) or excluded from the interval (interval is open). .
	 */
	@XmlAttribute(name = "inclusive")
	public Boolean inclusive;
	
	/**
	 * An attribute containing marshalled element node
	 */
	@XmlTransient
	private Node _xmlNodePresentation;
	
	
	/**
	 * Return inclusive.
	 * @return inclusive :  Specifies whether the limit is included in the interval (interval is closed) or excluded from the interval (interval is open). 
	 */
	public Boolean getInclusive() {
	    return inclusive;
	}
	
	/**
	 * Set a value to attribute inclusive.
	 * @param inclusive :  Specifies whether the limit is included in the interval (interval is closed) or excluded from the interval (interval is open). .
	 */
	public void setInclusive(Boolean inclusive) {
	    this.inclusive = inclusive;
	}
	
	
	/**
	 * Return inclusive.
	 * @return inclusive :  Specifies whether the limit is included in the interval (interval is closed) or excluded from the interval (interval is open). 
	 * Generated for the use on jsf pages
	 */
	 @Deprecated
	public Boolean isInclusive() {
	    return inclusive;
	}
	
	
	public Node get_xmlNodePresentation() {
		if (_xmlNodePresentation == null){
				JAXBContext jc;
				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
				dbf.setNamespaceAware(true);
				DocumentBuilder db = null;
				Document doc = null;
				try {
					db = dbf.newDocumentBuilder();
					doc = db.newDocument();
				} catch (ParserConfigurationException e1) {}
				try {
					jc = JAXBContext.newInstance("net.ihe.gazelle.ips.datatypes");
					Marshaller m = jc.createMarshaller();
					m.marshal(this, doc);
					_xmlNodePresentation = doc.getElementsByTagNameNS("urn:hl7-org:v3", "IVXB_TS").item(0);
				} catch (JAXBException e) {
					try{
						db = dbf.newDocumentBuilder();
						_xmlNodePresentation = db.newDocument();
					}
					catch(Exception ee){}
				}
			}
			return _xmlNodePresentation;
	}
	
	public void set_xmlNodePresentation(Node _xmlNodePresentation) {
		this._xmlNodePresentation = _xmlNodePresentation;
	}
	
	
	

	
	/**
     * validate by a module of validation
     * 
     */
   public static void validateByModule(IVXBTS iVXBTS, String _location, ConstraintValidatorModule cvm, List<net.ihe.gazelle.validation.Notification> diagnostic){
   		if (iVXBTS != null){
   			net.ihe.gazelle.ips.datatypes.TS.validateByModule(iVXBTS, _location, cvm, diagnostic);
    	}
    }

}