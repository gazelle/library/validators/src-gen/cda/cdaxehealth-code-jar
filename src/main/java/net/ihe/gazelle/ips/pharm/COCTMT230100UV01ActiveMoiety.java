package net.ihe.gazelle.ips.pharm;


import  net.ihe.gazelle.ips.datatypes.CS;
import net.ihe.gazelle.gen.common.ConstraintValidatorModule;
import net.ihe.gazelle.ips.infr.AllInfrastructureRootTemplateId;
import net.ihe.gazelle.voc.NullFlavor;
import net.ihe.gazelle.voc.RoleClassLocatedEntity;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Description of the class COCTMT230100UV01ActiveMoiety.
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "COCT_MT230100UV01.ActiveMoiety", propOrder = {
        "realmCode",
        "typeId",
        "templateId",
        "quantity",
        "activeMoiety",
        "nullFlavor",
        "classCode"
})
@XmlRootElement(name = "COCT_MT230100UV01.ActiveMoiety")
public class COCTMT230100UV01ActiveMoiety implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;


    @XmlElement(name = "realmCode", namespace = "urn:hl7-org:pharm")
    public List<CS> realmCode;
    @XmlElement(name = "typeId", namespace = "urn:hl7-org:pharm")
    public net.ihe.gazelle.ips.infr.AllInfrastructureRootTypeId typeId;
    @XmlElement(name = "templateId", namespace = "urn:hl7-org:pharm")
    public List<AllInfrastructureRootTemplateId> templateId;
    @XmlElement(name = "quantity", namespace = "urn:hl7-org:pharm")
    public net.ihe.gazelle.ips.datatypes.RTO quantity;
    @XmlElement(name = "activeMoiety", namespace = "urn:hl7-org:pharm")
    public List<net.ihe.gazelle.ips.pharm.COCTMT230100UV01ActiveMoietyEntity> activeMoiety;
    @XmlAttribute(name = "classCode", required = true)
    public RoleClassLocatedEntity classCode;
    @XmlAttribute(name = "nullFlavor")
    public NullFlavor nullFlavor;

    /**
     * An attribute containing marshalled element node
     */
    @XmlTransient
    private Node _xmlNodePresentation;


    /**
     * Return realmCode.
     *
     * @return realmCode
     */
    public List<CS> getRealmCode() {
        if (realmCode == null) {
            realmCode = new ArrayList<CS>();
        }
        return realmCode;
    }

    /**
     * Return typeId.
     *
     * @return typeId
     */
    public net.ihe.gazelle.ips.infr.AllInfrastructureRootTypeId getTypeId() {
        return typeId;
    }

    /**
     * Return templateId.
     *
     * @return templateId
     */


    public List<AllInfrastructureRootTemplateId> getTemplateId() {
        if (templateId == null) {
            templateId = new ArrayList<AllInfrastructureRootTemplateId>();
        }
        return templateId;
    }

    /**
     * Return quantity.
     *
     * @return quantity
     */


    public net.ihe.gazelle.ips.datatypes.RTO getQuantity() {
        return quantity;
    }

    /**
     * Return activeMoiety.
     *
     * @return activeMoiety
     */


    public List<net.ihe.gazelle.ips.pharm.COCTMT230100UV01ActiveMoietyEntity> getActiveMoiety() {
        if (activeMoiety == null) {
            activeMoiety = new ArrayList<COCTMT230100UV01ActiveMoietyEntity>();
        }
        return activeMoiety;
    }

    /**
     * Return classCode.
     *
     * @return classCode
     */


    public RoleClassLocatedEntity getClassCode() {
        return classCode;
    }



    /**
     * Return nullFlavor.
     *
     * @return nullFlavor
     */

    public NullFlavor getNullFlavor() {
        return nullFlavor;
    }

    /**
     * Set realmCode.
     *
     * @param realmCode
     *            the new value for realmCode
     */
    public void setRealmCode(List<CS> realmCode) {
        this.realmCode = realmCode;
    }

    /**
     * Set typeId.
     *
     * @param typeId
     *            the new value for typeId
     */

    public void setTypeId(net.ihe.gazelle.ips.infr.AllInfrastructureRootTypeId typeId) {
        this.typeId = typeId;
    }

    /**
     * Set templateId.
     *
     * @param templateId
     *            the new value for templateId
     */

    public void setTemplateId(List<AllInfrastructureRootTemplateId> templateId) {
        this.templateId = templateId;
    }

    /**
     * Set quantity.
     *
     * @param quantity
     *            the new value for quantity
     */

    public void setQuantity(net.ihe.gazelle.ips.datatypes.RTO quantity) {
        this.quantity = quantity;
    }

    /**
     * Set activeMoiety.
     *
     * @param activeMoiety
     *            the new value for activeMoiety
     */

    public void setActiveMoiety(List<COCTMT230100UV01ActiveMoietyEntity> activeMoiety) {
        this.activeMoiety = activeMoiety;
    }

    /**
     * Set classCode.
     *
     * @param classCode
     *            the new value for classCode
     */

    public void setClassCode(RoleClassLocatedEntity classCode) {
        this.classCode = classCode;
    }

    /**
     * Set nullFlavor.
     *
     * @param nullFlavor
     *            the new value for nullFlavor
     */

    public void setNullFlavor(NullFlavor nullFlavor) {
        this.nullFlavor = nullFlavor;
    }

    public Node get_xmlNodePresentation() {
        if (_xmlNodePresentation == null){
            JAXBContext jc;
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setNamespaceAware(true);
            DocumentBuilder db = null;
            Document doc = null;
            try {
                db = dbf.newDocumentBuilder();
                doc = db.newDocument();
            } catch (ParserConfigurationException e1) {}
            try {
                jc = JAXBContext.newInstance("net.ihe.gazelle.ips.pharm");
                Marshaller m = jc.createMarshaller();
                m.marshal(this, doc);
                _xmlNodePresentation = doc.getElementsByTagNameNS("urn:hl7-org:pharm", "COCT_MT230100UV01.ActiveMoiety").item(0);
            } catch (JAXBException e) {
                try{
                    db = dbf.newDocumentBuilder();
                    _xmlNodePresentation = db.newDocument();
                }
                catch(Exception ee){}
            }
        }
        return _xmlNodePresentation;
    }


    /**
     * validate by a module of validation
     *
     */
    public static void validateByModule(net.ihe.gazelle.ips.pharm.COCTMT230100UV01ActiveMoiety cOCTMT230100UV01ActiveMoiety, String _location, ConstraintValidatorModule cvm, List<net.ihe.gazelle.validation.Notification> diagnostic){
        if (cOCTMT230100UV01ActiveMoiety != null){
            cvm.validate(cOCTMT230100UV01ActiveMoiety, _location, diagnostic);
            {
                int i = 0;
                for (CS realmCode: cOCTMT230100UV01ActiveMoiety.getRealmCode()){
                    CS .validateByModule(realmCode, _location + "/realmCode[" + i + "]", cvm, diagnostic);
                    i++;
                }
            }

            net.ihe.gazelle.ips.infr.AllInfrastructureRootTypeId.validateByModule(cOCTMT230100UV01ActiveMoiety.getTypeId(), _location + "/typeId", cvm, diagnostic);
            {
                int i = 0;
                for (AllInfrastructureRootTemplateId templateId: cOCTMT230100UV01ActiveMoiety.getTemplateId()){
                    AllInfrastructureRootTemplateId.validateByModule(templateId, _location + "/templateId[" + i + "]", cvm, diagnostic);
                    i++;
                }
            }
            net.ihe.gazelle.ips.datatypes.RTO.validateByModule(cOCTMT230100UV01ActiveMoiety.getTypeId(), _location + "/quantity", cvm, diagnostic);

            {
                int i = 0;
                for (COCTMT230100UV01ActiveMoietyEntity activeMoiety: cOCTMT230100UV01ActiveMoiety.getActiveMoiety()){
                    COCTMT230100UV01ActiveMoietyEntity.validateByModule(activeMoiety, _location + "/asSpecializedKind[" + i + "]", cvm, diagnostic);
                    i++;
                }
            }
        }
    }


}
