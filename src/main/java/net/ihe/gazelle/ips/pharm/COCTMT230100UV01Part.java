package net.ihe.gazelle.ips.pharm;


import  net.ihe.gazelle.ips.datatypes.CS;
import net.ihe.gazelle.ips.datatypes.II;
import net.ihe.gazelle.gen.common.ConstraintValidatorModule;
import net.ihe.gazelle.voc.NullFlavor;
import net.ihe.gazelle.voc.RoleClass;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.util.ArrayList;
import java.util.List;

/**
 * Description of the class COCTMT230100UV01Part.
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "COCT_MT230100UV01.Part", propOrder = {
        "realmCode",
        "typeId",
        "templateId",
        "id",
        "quantity",
        "partProduct",
        "subjectOf",
        "classCode",
        "nullFlavor"
})
@XmlRootElement(name = "COCT_MT230100UV01.Part")
public class COCTMT230100UV01Part implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;


    @XmlElement(name = "realmCode", namespace = "urn:hl7-org:pharm")
    public List<CS> realmCode;
    @XmlElement(name = "typeId", namespace = "urn:hl7-org:pharm")
    public net.ihe.gazelle.ips.infr.AllInfrastructureRootTypeId typeId;
    @XmlElement(name = "templateId", namespace = "urn:hl7-org:pharm")
    public List<net.ihe.gazelle.ips.infr.AllInfrastructureRootTemplateId> templateId;
    @XmlElement(name= "id", namespace = "urn:hl7-org:pharm")
    public II id;
    @XmlElement(name = "quantity", namespace = "urn:hl7-org:pharm")
    public net.ihe.gazelle.ips.datatypes.PQ quantity;
    @XmlElement(name = "partProduct", required = true, namespace = "urn:hl7-org:pharm")
    public net.ihe.gazelle.ips.pharm.COCTMT230100UV01Product partProduct;
    @XmlElement(name = "subjectOf", namespace = "urn:hl7-org:pharm")
    public List<net.ihe.gazelle.ips.pharm.COCTMT230100UV01Subject6> subjectOf;
    @XmlAttribute(name = "nullFlavor")
    public NullFlavor nullFlavor;
    @XmlAttribute(name = "classCode", required = true)
    public RoleClass classCode;

    /**
     * An attribute containing marshalled element node
     */
    @XmlTransient
    private Node _xmlNodePresentation;


    /**
     * Return realmCode.
     * @return realmCode
     */
    public List<CS> getRealmCode() {
        if (realmCode == null) {
            realmCode = new ArrayList<CS>();
        }
        return realmCode;
    }

    /**
     * Set a value to attribute realmCode.
     * @param realmCode.
     */
    public void setRealmCode(List<CS> realmCode) {
        this.realmCode = realmCode;
    }



    /**
     * Add a realmCode to the realmCode collection.
     * @param realmCode_elt Element to add.
     */
    public void addRealmCode(CS realmCode_elt) {
        this.getRealmCode().add(realmCode_elt);
    }

    /**
     * Remove a realmCode to the realmCode collection.
     * @param realmCode_elt Element to remove
     */
    public void removeRealmCode(CS realmCode_elt) {
        this.getRealmCode().remove(realmCode_elt);
    }

    /**
     * Return typeId.
     * @return typeId
     */
    public net.ihe.gazelle.ips.infr.AllInfrastructureRootTypeId getTypeId() {
        return typeId;
    }

    /**
     * Set a value to attribute typeId.
     * @param typeId.
     */
    public void setTypeId(net.ihe.gazelle.ips.infr.AllInfrastructureRootTypeId typeId) {
        this.typeId = typeId;
    }




    /**
     * Return templateId.
     * @return templateId
     */
    public List<net.ihe.gazelle.ips.infr.AllInfrastructureRootTemplateId> getTemplateId() {
        if (templateId == null) {
            templateId = new ArrayList<net.ihe.gazelle.ips.infr.AllInfrastructureRootTemplateId>();
        }
        return templateId;
    }

    /**
     * Set a value to attribute templateId.
     * @param templateId.
     */
    public void setTemplateId(List<net.ihe.gazelle.ips.infr.AllInfrastructureRootTemplateId> templateId) {
        this.templateId = templateId;
    }



    /**
     * Add a templateId to the templateId collection.
     * @param templateId_elt Element to add.
     */
    public void addTemplateId(net.ihe.gazelle.ips.infr.AllInfrastructureRootTemplateId templateId_elt) {
        this.getTemplateId().add(templateId_elt);
    }

    /**
     * Remove a templateId to the templateId collection.
     * @param templateId_elt Element to remove
     */
    public void removeTemplateId(net.ihe.gazelle.ips.infr.AllInfrastructureRootTemplateId templateId_elt) {
        this.getTemplateId().remove(templateId_elt);
    }

    public II getId() {
        return id;
    }

    public COCTMT230100UV01Part setId(II id) {
        this.id = id;
        return this;
    }

    /**
     * Return quantity.
     * @return quantity
     */
    public net.ihe.gazelle.ips.datatypes.PQ getQuantity() {
        return quantity;
    }

    /**
     * Set a value to attribute quantity.
     * @param quantity.
     */
    public void setQuantity(net.ihe.gazelle.ips.datatypes.PQ quantity) {
        this.quantity = quantity;
    }




    /**
     * Return partMedicine.
     * @return partMedicine
     */
    public net.ihe.gazelle.ips.pharm.COCTMT230100UV01Product getPartProduct() {
        return partProduct;
    }

    /**
     * Set a value to attribute partMedicine.
     * @param partMedicine.
     */
    public void setPartProduct(net.ihe.gazelle.ips.pharm.COCTMT230100UV01Product partProduct) {
        this.partProduct = partProduct;
    }




    /**
     * Return subjectOf.
     * @return subjectOf
     */
    public List<net.ihe.gazelle.ips.pharm.COCTMT230100UV01Subject6> getSubjectOf() {
        if (subjectOf == null) {
            subjectOf = new ArrayList<net.ihe.gazelle.ips.pharm.COCTMT230100UV01Subject6>();
        }
        return subjectOf;
    }

    /**
     * Set a value to attribute subjectOf.
     * @param subjectOf.
     */
    public void setSubjectOf(List<net.ihe.gazelle.ips.pharm.COCTMT230100UV01Subject6> subjectOf) {
        this.subjectOf = subjectOf;
    }



    /**
     * Add a subjectOf to the subjectOf collection.
     * @param subjectOf_elt Element to add.
     */
    public void addSubjectOf(net.ihe.gazelle.ips.pharm.COCTMT230100UV01Subject6 subjectOf_elt) {
        this.getSubjectOf().add(subjectOf_elt);
    }

    /**
     * Remove a subjectOf to the subjectOf collection.
     * @param subjectOf_elt Element to remove
     */
    public void removeSubjectOf(net.ihe.gazelle.ips.pharm.COCTMT230100UV01Subject6 subjectOf_elt) {
        this.getSubjectOf().remove(subjectOf_elt);
    }

    /**
     * Return classCode.
     * @return classCode
     */
    public RoleClass getClassCode() {
        return classCode;
    }

    /**
     * Set a value to attribute classCode.
     * @param classCode.
     */
    public void setClassCode(RoleClass classCode) {
        this.classCode = classCode;
    }




    /**
     * Return nullFlavor.
     * @return nullFlavor
     */
    public NullFlavor getNullFlavor() {
        return nullFlavor;
    }

    /**
     * Set a value to attribute nullFlavor.
     * @param nullFlavor.
     */
    public void setNullFlavor(NullFlavor nullFlavor) {
        this.nullFlavor = nullFlavor;
    }





    public Node get_xmlNodePresentation() {
        if (_xmlNodePresentation == null){
            JAXBContext jc;
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setNamespaceAware(true);
            DocumentBuilder db = null;
            Document doc = null;
            try {
                db = dbf.newDocumentBuilder();
                doc = db.newDocument();
            } catch (ParserConfigurationException e1) {}
            try {
                jc = JAXBContext.newInstance("net.ihe.gazelle.ips.pharm");
                Marshaller m = jc.createMarshaller();
                m.marshal(this, doc);
                _xmlNodePresentation = doc.getElementsByTagNameNS("urn:hl7-org:pharm", "COCT_MT230100UV01.Part").item(0);
            } catch (JAXBException e) {
                try{
                    db = dbf.newDocumentBuilder();
                    _xmlNodePresentation = db.newDocument();
                }
                catch(Exception ee){}
            }
        }
        return _xmlNodePresentation;
    }

    public void set_xmlNodePresentation(Node _xmlNodePresentation) {
        this._xmlNodePresentation = _xmlNodePresentation;
    }





    /**
     * validate by a module of validation
     *
     */
    public static void validateByModule(net.ihe.gazelle.ips.pharm.COCTMT230100UV01Part cOCTMT230100UV01Part, String _location, ConstraintValidatorModule cvm, List<net.ihe.gazelle.validation.Notification> diagnostic){
        if (cOCTMT230100UV01Part != null){
            cvm.validate(cOCTMT230100UV01Part, _location, diagnostic);
            {
                int i = 0;
                for (CS realmCode: cOCTMT230100UV01Part.getRealmCode()){
                    CS .validateByModule(realmCode, _location + "/realmCode[" + i + "]", cvm, diagnostic);
                    i++;
                }
            }

            net.ihe.gazelle.ips.infr.AllInfrastructureRootTypeId.validateByModule(cOCTMT230100UV01Part.getTypeId(), _location + "/typeId", cvm, diagnostic);
            {
                int i = 0;
                for (net.ihe.gazelle.ips.infr.AllInfrastructureRootTemplateId templateId: cOCTMT230100UV01Part.getTemplateId()){
                    net.ihe.gazelle.ips.infr.AllInfrastructureRootTemplateId.validateByModule(templateId, _location + "/templateId[" + i + "]", cvm, diagnostic);
                    i++;
                }
            }

            net.ihe.gazelle.ips.datatypes.PQ.validateByModule(cOCTMT230100UV01Part.getQuantity(), _location + "/quantity", cvm, diagnostic);
            net.ihe.gazelle.ips.pharm.COCTMT230100UV01Product.validateByModule(cOCTMT230100UV01Part.getPartProduct(), _location + "/partProduct", cvm, diagnostic);
            {
                int i = 0;
                for (net.ihe.gazelle.ips.pharm.COCTMT230100UV01Subject6 subjectOf: cOCTMT230100UV01Part.getSubjectOf()){
                    net.ihe.gazelle.ips.pharm.COCTMT230100UV01Subject6.validateByModule(subjectOf, _location + "/subjectOf[" + i + "]", cvm, diagnostic);
                    i++;
                }
            }

        }
    }

}
