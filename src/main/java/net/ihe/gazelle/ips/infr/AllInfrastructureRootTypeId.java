/**
 * AllInfrastructureRootTypeId.java
 *
 * File generated from the infr::AllInfrastructureRootTypeId uml Class
 * Generated by the Acceleo UML 2.1 to Java generator module (Obeo)
 */
package net.ihe.gazelle.ips.infr;

// End of user code

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.util.List;


/**
 * Description of the class AllInfrastructureRootTypeId.
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "all.InfrastructureRoot.typeId")
@XmlRootElement(name = "all.InfrastructureRoot.typeId")
public class AllInfrastructureRootTypeId extends net.ihe.gazelle.ips.datatypes.II implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	
	/**
	 * An attribute containing marshalled element node
	 */
	@XmlTransient
	private Node _xmlNodePresentation;
	
	
	
	public Node get_xmlNodePresentation() {
		if (_xmlNodePresentation == null){
				JAXBContext jc;
				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
				dbf.setNamespaceAware(true);
				DocumentBuilder db = null;
				Document doc = null;
				try {
					db = dbf.newDocumentBuilder();
					doc = db.newDocument();
				} catch (ParserConfigurationException e1) {}
				try {
					jc = JAXBContext.newInstance("net.ihe.gazelle.ips.infr");
					Marshaller m = jc.createMarshaller();
					m.marshal(this, doc);
					_xmlNodePresentation = doc.getElementsByTagNameNS("urn:hl7-org:v3", "all.InfrastructureRoot.typeId").item(0);
				} catch (JAXBException e) {
					try{
						db = dbf.newDocumentBuilder();
						_xmlNodePresentation = db.newDocument();
					}
					catch(Exception ee){}
				}
			}
			return _xmlNodePresentation;
	}
	
	public void set_xmlNodePresentation(Node _xmlNodePresentation) {
		this._xmlNodePresentation = _xmlNodePresentation;
	}
	
	

	
	/**
     * validate by a module of validation
     * 
     */
   public static void validateByModule(AllInfrastructureRootTypeId allInfrastructureRootTypeId, String location, ConstraintValidatorModule cvm, List<net.ihe.gazelle.validation.Notification> diagnostic){
   		if (allInfrastructureRootTypeId != null){
   			net.ihe.gazelle.ips.datatypes.II.validateByModule(allInfrastructureRootTypeId, location, cvm, diagnostic);
    	}
    }

}