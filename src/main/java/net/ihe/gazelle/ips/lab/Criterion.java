package net.ihe.gazelle.ips.lab;


import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;

import net.ihe.gazelle.ips.datatypes.CD;
import org.w3c.dom.Document;
import org.w3c.dom.Node;


/**
 * Description of the class Criterion.
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "POCD_MT000040.Criterion", propOrder = {
        "code",
        "value",
        "classCode",
        "moodCode",
        "nullFlavor"
})
@XmlRootElement(name = "Criterion")
public class Criterion implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @XmlElement(name = "code", namespace = "urn:oid:1.3.6.1.4.1.19376.1.3.2")
    public CD code;
    @XmlElement(name = "value", namespace = "urn:oid:1.3.6.1.4.1.19376.1.3.2")
    public net.ihe.gazelle.ips.datatypes.ANY value;
    @XmlAttribute(name = "classCode")
    public net.ihe.gazelle.voc.ActClassObservation classCode;
    @XmlAttribute(name = "moodCode")
    public net.ihe.gazelle.voc.ActMood moodCode;
    @XmlAttribute(name = "nullFlavor")
    public net.ihe.gazelle.voc.NullFlavor nullFlavor;

    /**
     * An attribute containing marshalled element node
     */
    @XmlTransient
    private Node _xmlNodePresentation;

    /**
     * Return code.
     * @return code
     */
    public CD getCode() {
        return code;
    }

    /**
     * Set a value to attribute code.
     * @param code.
     */
    public void setCode(CD code) {
        this.code = code;
    }

    /**
     * Return value.
     * @return value
     */
    public net.ihe.gazelle.ips.datatypes.ANY getValue() {
        return value;
    }

    /**
     * Set a value to attribute value.
     * @param value.
     */
    public void setValue(net.ihe.gazelle.ips.datatypes.ANY value) {
        this.value = value;
    }




    /**
     * Return classCode.
     * @return classCode
     */
    public net.ihe.gazelle.voc.ActClassObservation getClassCode() {
        return classCode;
    }

    /**
     * Set a value to attribute classCode.
     * @param classCode.
     */
    public void setClassCode(net.ihe.gazelle.voc.ActClassObservation classCode) {
        this.classCode = classCode;
    }




    /**
     * Return moodCode.
     * @return moodCode
     */
    public net.ihe.gazelle.voc.ActMood getMoodCode() {
        return moodCode;
    }

    /**
     * Set a value to attribute moodCode.
     * @param moodCode.
     */
    public void setMoodCode(net.ihe.gazelle.voc.ActMood moodCode) {
        this.moodCode = moodCode;
    }




    /**
     * Return nullFlavor.
     * @return nullFlavor
     */
    public net.ihe.gazelle.voc.NullFlavor getNullFlavor() {
        return nullFlavor;
    }

    /**
     * Set a value to attribute nullFlavor.
     * @param nullFlavor.
     */
    public void setNullFlavor(net.ihe.gazelle.voc.NullFlavor nullFlavor) {
        this.nullFlavor = nullFlavor;
    }





    public Node get_xmlNodePresentation() {
        if (_xmlNodePresentation == null){
            JAXBContext jc;
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setNamespaceAware(true);
            DocumentBuilder db = null;
            Document doc = null;
            try {
                db = dbf.newDocumentBuilder();
                doc = db.newDocument();
            } catch (ParserConfigurationException e1) {}
            try {
                jc = JAXBContext.newInstance("net/ihe/gazelle/ips/lab");
                Marshaller m = jc.createMarshaller();
                m.marshal(this, doc);
                _xmlNodePresentation = doc.getElementsByTagNameNS("urn:oid:1.3.6.1.4.1.19376.1.3.2", "Criterion").item(0);
            } catch (JAXBException e) {
                try{
                    db = dbf.newDocumentBuilder();
                    _xmlNodePresentation = db.newDocument();
                }
                catch(Exception ee){}
            }
        }
        return _xmlNodePresentation;
    }

    public void set_xmlNodePresentation(Node _xmlNodePresentation) {
        this._xmlNodePresentation = _xmlNodePresentation;
    }





    /**
     * validate by a module of validation
     *
     */
    public static void validateByModule(Criterion pOCDMT000040Criterion, String _location, ConstraintValidatorModule cvm, List<net.ihe.gazelle.validation.Notification> diagnostic){
        if (pOCDMT000040Criterion != null){
            cvm.validate(pOCDMT000040Criterion, _location, diagnostic);
            CD.validateByModule(pOCDMT000040Criterion.getCode(), _location + "/code", cvm, diagnostic);
            net.ihe.gazelle.ips.datatypes.ANY.validateByModule(pOCDMT000040Criterion.getValue(), _location + "/value", cvm, diagnostic);
        }
    }

}