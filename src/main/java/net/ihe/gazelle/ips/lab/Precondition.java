package net.ihe.gazelle.ips.lab;

/**
 * POCDMT000040Precondition.java
 *
 */

import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;

import org.w3c.dom.Document;
import org.w3c.dom.Node;


/**
 * Description of the class Precondition.
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "POCD_MT000040.Precondition", propOrder = {
        "criterion",
        "nullFlavor",
        "typeCode"
})
@XmlRootElement(name = "Precondition")
public class Precondition implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @XmlElement(name = "criterion", required = true, namespace = "urn:oid:1.3.6.1.4.1.19376.1.3.2")
    public Criterion criterion;
    @XmlAttribute(name = "nullFlavor")
    public net.ihe.gazelle.voc.NullFlavor nullFlavor;
    @XmlAttribute(name = "typeCode")
    public net.ihe.gazelle.voc.ActRelationshipType typeCode;

    /**
     * An attribute containing marshalled element node
     */
    @XmlTransient
    private Node _xmlNodePresentation;


    /**
     * Return criterion.
     * @return criterion
     */
    public Criterion getCriterion() {
        return criterion;
    }

    /**
     * Set a value to attribute criterion.
     * @param criterion.
     */
    public void setCriterion(Criterion criterion) {
        this.criterion = criterion;
    }




    /**
     * Return nullFlavor.
     * @return nullFlavor
     */
    public net.ihe.gazelle.voc.NullFlavor getNullFlavor() {
        return nullFlavor;
    }

    /**
     * Set a value to attribute nullFlavor.
     * @param nullFlavor.
     */
    public void setNullFlavor(net.ihe.gazelle.voc.NullFlavor nullFlavor) {
        this.nullFlavor = nullFlavor;
    }




    /**
     * Return typeCode.
     * @return typeCode
     */
    public net.ihe.gazelle.voc.ActRelationshipType getTypeCode() {
        return typeCode;
    }

    /**
     * Set a value to attribute typeCode.
     * @param typeCode.
     */
    public void setTypeCode(net.ihe.gazelle.voc.ActRelationshipType typeCode) {
        this.typeCode = typeCode;
    }





    public Node get_xmlNodePresentation() {
        if (_xmlNodePresentation == null){
            JAXBContext jc;
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setNamespaceAware(true);
            DocumentBuilder db = null;
            Document doc = null;
            try {
                db = dbf.newDocumentBuilder();
                doc = db.newDocument();
            } catch (ParserConfigurationException e1) {}
            try {
                jc = JAXBContext.newInstance("net/ihe/gazelle/ips/lab");
                Marshaller m = jc.createMarshaller();
                m.marshal(this, doc);
                _xmlNodePresentation = doc.getElementsByTagNameNS("urn:oid:1.3.6.1.4.1.19376.1.3.2", "Precondition").item(0);
            } catch (JAXBException e) {
                try{
                    db = dbf.newDocumentBuilder();
                    _xmlNodePresentation = db.newDocument();
                }
                catch(Exception ee){}
            }
        }
        return _xmlNodePresentation;
    }

    public void set_xmlNodePresentation(Node _xmlNodePresentation) {
        this._xmlNodePresentation = _xmlNodePresentation;
    }





    /**
     * validate by a module of validation
     *
     */
    public static void validateByModule(Precondition pOCDMT000040Precondition, String _location, ConstraintValidatorModule cvm, List<net.ihe.gazelle.validation.Notification> diagnostic){
        if (pOCDMT000040Precondition != null){
            cvm.validate(pOCDMT000040Precondition, _location, diagnostic);
            Criterion.validateByModule(pOCDMT000040Precondition.getCriterion(), _location + "/criterion", cvm, diagnostic);
        }
    }

}
