/**
 * POCDMT000040AuthoringDevice.java
 *
 * File generated from the cda::POCDMT000040AuthoringDevice uml Class
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.cdaxehealth;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;

import net.ihe.gazelle.ips.datatypes.CE;
import net.ihe.gazelle.ips.datatypes.CS;
import org.w3c.dom.Document;
import org.w3c.dom.Node;


/**
 * Description of the class POCDMT000040AuthoringDevice.
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "POCD_MT000040.AuthoringDevice", propOrder = {
	"realmCode",
	"typeId",
	"templateId",
	"code",
	"manufacturerModelName",
	"softwareName",
	"asMaintainedEntity",
	"classCode",
	"determinerCode",
	"nullFlavor"
})
@XmlRootElement(name = "POCD_MT000040.AuthoringDevice")
public class POCDMT000040AuthoringDevice implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@XmlElement(name = "realmCode", namespace = "urn:hl7-org:v3")
	public List<CS > realmCode;
	@XmlElement(name = "typeId", namespace = "urn:hl7-org:v3")
	public POCDMT000040InfrastructureRootTypeId typeId;
	@XmlElement(name = "templateId", namespace = "urn:hl7-org:v3")
	public List<net.ihe.gazelle.ips.datatypes.II> templateId;
	@XmlElement(name = "code", namespace = "urn:hl7-org:v3")
	public CE code;
	@XmlElement(name = "manufacturerModelName", namespace = "urn:hl7-org:v3")
	public net.ihe.gazelle.ips.datatypes.SC manufacturerModelName;
	@XmlElement(name = "softwareName", namespace = "urn:hl7-org:v3")
	public net.ihe.gazelle.ips.datatypes.SC softwareName;
	@XmlElement(name = "asMaintainedEntity", namespace = "urn:hl7-org:v3")
	public List<POCDMT000040MaintainedEntity> asMaintainedEntity;
	@XmlAttribute(name = "classCode")
	public net.ihe.gazelle.voc.EntityClassDevice classCode;
	@XmlAttribute(name = "determinerCode")
	public net.ihe.gazelle.voc.EntityDeterminer determinerCode;
	@XmlAttribute(name = "nullFlavor")
	public net.ihe.gazelle.voc.NullFlavor nullFlavor;
	
	/**
	 * An attribute containing marshalled element node
	 */
	@XmlTransient
	private Node _xmlNodePresentation;
	
	
	/**
	 * Return realmCode.
	 * @return realmCode
	 */
	public List<CS > getRealmCode() {
		if (realmCode == null) {
	        realmCode = new ArrayList<CS >();
	    }
	    return realmCode;
	}
	
	/**
	 * Set a value to attribute realmCode.
	 * @param realmCode.
	 */
	public void setRealmCode(List<CS > realmCode) {
	    this.realmCode = realmCode;
	}
	
	
	
	/**
	 * Add a realmCode to the realmCode collection.
	 * @param realmCode_elt Element to add.
	 */
	public void addRealmCode(CS realmCode_elt) {
	    this.getRealmCode().add(realmCode_elt);
	}
	
	/**
	 * Remove a realmCode to the realmCode collection.
	 * @param realmCode_elt Element to remove
	 */
	public void removeRealmCode(CS realmCode_elt) {
	    this.getRealmCode().remove(realmCode_elt);
	}
	
	/**
	 * Return typeId.
	 * @return typeId
	 */
	public POCDMT000040InfrastructureRootTypeId getTypeId() {
	    return typeId;
	}
	
	/**
	 * Set a value to attribute typeId.
	 * @param typeId.
	 */
	public void setTypeId(POCDMT000040InfrastructureRootTypeId typeId) {
	    this.typeId = typeId;
	}
	
	
	
	
	/**
	 * Return templateId.
	 * @return templateId
	 */
	public List<net.ihe.gazelle.ips.datatypes.II> getTemplateId() {
		if (templateId == null) {
	        templateId = new ArrayList<net.ihe.gazelle.ips.datatypes.II>();
	    }
	    return templateId;
	}
	
	/**
	 * Set a value to attribute templateId.
	 * @param templateId.
	 */
	public void setTemplateId(List<net.ihe.gazelle.ips.datatypes.II> templateId) {
	    this.templateId = templateId;
	}
	
	
	
	/**
	 * Add a templateId to the templateId collection.
	 * @param templateId_elt Element to add.
	 */
	public void addTemplateId(net.ihe.gazelle.ips.datatypes.II templateId_elt) {
	    this.getTemplateId().add(templateId_elt);
	}
	
	/**
	 * Remove a templateId to the templateId collection.
	 * @param templateId_elt Element to remove
	 */
	public void removeTemplateId(net.ihe.gazelle.ips.datatypes.II templateId_elt) {
	    this.getTemplateId().remove(templateId_elt);
	}
	
	/**
	 * Return code.
	 * @return code
	 */
	public CE getCode() {
	    return code;
	}
	
	/**
	 * Set a value to attribute code.
	 * @param code.
	 */
	public void setCode(CE code) {
	    this.code = code;
	}
	
	
	
	
	/**
	 * Return manufacturerModelName.
	 * @return manufacturerModelName
	 */
	public net.ihe.gazelle.ips.datatypes.SC getManufacturerModelName() {
	    return manufacturerModelName;
	}
	
	/**
	 * Set a value to attribute manufacturerModelName.
	 * @param manufacturerModelName.
	 */
	public void setManufacturerModelName(net.ihe.gazelle.ips.datatypes.SC manufacturerModelName) {
	    this.manufacturerModelName = manufacturerModelName;
	}
	
	
	
	
	/**
	 * Return softwareName.
	 * @return softwareName
	 */
	public net.ihe.gazelle.ips.datatypes.SC getSoftwareName() {
	    return softwareName;
	}
	
	/**
	 * Set a value to attribute softwareName.
	 * @param softwareName.
	 */
	public void setSoftwareName(net.ihe.gazelle.ips.datatypes.SC softwareName) {
	    this.softwareName = softwareName;
	}
	
	
	
	
	/**
	 * Return asMaintainedEntity.
	 * @return asMaintainedEntity
	 */
	public List<POCDMT000040MaintainedEntity> getAsMaintainedEntity() {
		if (asMaintainedEntity == null) {
	        asMaintainedEntity = new ArrayList<POCDMT000040MaintainedEntity>();
	    }
	    return asMaintainedEntity;
	}
	
	/**
	 * Set a value to attribute asMaintainedEntity.
	 * @param asMaintainedEntity.
	 */
	public void setAsMaintainedEntity(List<POCDMT000040MaintainedEntity> asMaintainedEntity) {
	    this.asMaintainedEntity = asMaintainedEntity;
	}
	
	
	
	/**
	 * Add a asMaintainedEntity to the asMaintainedEntity collection.
	 * @param asMaintainedEntity_elt Element to add.
	 */
	public void addAsMaintainedEntity(POCDMT000040MaintainedEntity asMaintainedEntity_elt) {
	    this.getAsMaintainedEntity().add(asMaintainedEntity_elt);
	}
	
	/**
	 * Remove a asMaintainedEntity to the asMaintainedEntity collection.
	 * @param asMaintainedEntity_elt Element to remove
	 */
	public void removeAsMaintainedEntity(POCDMT000040MaintainedEntity asMaintainedEntity_elt) {
	    this.getAsMaintainedEntity().remove(asMaintainedEntity_elt);
	}
	
	/**
	 * Return classCode.
	 * @return classCode
	 */
	public net.ihe.gazelle.voc.EntityClassDevice getClassCode() {
	    return classCode;
	}
	
	/**
	 * Set a value to attribute classCode.
	 * @param classCode.
	 */
	public void setClassCode(net.ihe.gazelle.voc.EntityClassDevice classCode) {
	    this.classCode = classCode;
	}
	
	
	
	
	/**
	 * Return determinerCode.
	 * @return determinerCode
	 */
	public net.ihe.gazelle.voc.EntityDeterminer getDeterminerCode() {
	    return determinerCode;
	}
	
	/**
	 * Set a value to attribute determinerCode.
	 * @param determinerCode.
	 */
	public void setDeterminerCode(net.ihe.gazelle.voc.EntityDeterminer determinerCode) {
	    this.determinerCode = determinerCode;
	}
	
	
	
	
	/**
	 * Return nullFlavor.
	 * @return nullFlavor
	 */
	public net.ihe.gazelle.voc.NullFlavor getNullFlavor() {
	    return nullFlavor;
	}
	
	/**
	 * Set a value to attribute nullFlavor.
	 * @param nullFlavor.
	 */
	public void setNullFlavor(net.ihe.gazelle.voc.NullFlavor nullFlavor) {
	    this.nullFlavor = nullFlavor;
	}
	
	
	
	
	
	public Node get_xmlNodePresentation() {
		if (_xmlNodePresentation == null){
				JAXBContext jc;
				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
				dbf.setNamespaceAware(true);
				DocumentBuilder db = null;
				Document doc = null;
				try {
					db = dbf.newDocumentBuilder();
					doc = db.newDocument();
				} catch (ParserConfigurationException e1) {}
				try {
					jc = JAXBContext.newInstance("net.ihe.gazelle.cdaxehealth");
					Marshaller m = jc.createMarshaller();
					m.marshal(this, doc);
					_xmlNodePresentation = doc.getElementsByTagNameNS("urn:hl7-org:v3", "POCD_MT000040.AuthoringDevice").item(0);
				} catch (JAXBException e) {
					try{
						db = dbf.newDocumentBuilder();
						_xmlNodePresentation = db.newDocument();
					}
					catch(Exception ee){}
				}
			}
			return _xmlNodePresentation;
	}
	
	public void set_xmlNodePresentation(Node _xmlNodePresentation) {
		this._xmlNodePresentation = _xmlNodePresentation;
	}
	
	
	

	
	/**
     * validate by a module of validation
     * 
     */
   public static void validateByModule(POCDMT000040AuthoringDevice pOCDMT000040AuthoringDevice, String _location, ConstraintValidatorModule cvm, List<net.ihe.gazelle.validation.Notification> diagnostic){
   		if (pOCDMT000040AuthoringDevice != null){
   			cvm.validate(pOCDMT000040AuthoringDevice, _location, diagnostic);
			{
				int i = 0;
				for (CS realmCode: pOCDMT000040AuthoringDevice.getRealmCode()){
					CS .validateByModule(realmCode, _location + "/realmCode[" + i + "]", cvm, diagnostic);
					i++;
				}
			}
			
			POCDMT000040InfrastructureRootTypeId.validateByModule(pOCDMT000040AuthoringDevice.getTypeId(), _location + "/typeId", cvm, diagnostic);
			{
				int i = 0;
				for (net.ihe.gazelle.ips.datatypes.II templateId: pOCDMT000040AuthoringDevice.getTemplateId()){
					net.ihe.gazelle.ips.datatypes.II.validateByModule(templateId, _location + "/templateId[" + i + "]", cvm, diagnostic);
					i++;
				}
			}
			
			CE .validateByModule(pOCDMT000040AuthoringDevice.getCode(), _location + "/code", cvm, diagnostic);
			net.ihe.gazelle.ips.datatypes.SC.validateByModule(pOCDMT000040AuthoringDevice.getManufacturerModelName(), _location + "/manufacturerModelName", cvm, diagnostic);
			net.ihe.gazelle.ips.datatypes.SC.validateByModule(pOCDMT000040AuthoringDevice.getSoftwareName(), _location + "/softwareName", cvm, diagnostic);
			{
				int i = 0;
				for (POCDMT000040MaintainedEntity asMaintainedEntity: pOCDMT000040AuthoringDevice.getAsMaintainedEntity()){
					POCDMT000040MaintainedEntity.validateByModule(asMaintainedEntity, _location + "/asMaintainedEntity[" + i + "]", cvm, diagnostic);
					i++;
				}
			}
			
    	}
    }

}